import React from 'react';
import './VotingScreen.css'
import {PlayerVote} from "../player_vote/PlayerVote";
import SockJsClient from "react-stomp";
import {Chat} from "../chat/Chat";

export class VotingScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            game: JSON.parse(localStorage.getItem("game")),
            url: 'https://ip2-maffia.herokuapp.com/game?token=' + localStorage.getItem('JwtToken'),
            votes: null,
            voters: [],
            role: localStorage.getItem("role"),
            readyCount: 0,
            placeholder: "",
            allowedNoVote: false,
            chatRole: ""
        };
        this.componentWillMount = this.componentWillMount.bind(this);
        this.handleMessage = this.handleMessage.bind(this);
        this.handleClick = this.handleClick.bind(this);
        this.changeRouteGame = this.changeRouteGame.bind(this);
        this.readyVote = this.readyVote.bind(this);
        this.backButtonFun = this.backButtonFun.bind(this);
        this.readyCounter = this.readyCounter.bind(this);
        this.powerHandle = this.powerHandle.bind(this);
        this.handleCitizenNight = this.handleCitizenNight.bind(this);
        this.loadData = this.loadData.bind(this);
    }

    changeRouteGame() {
        let path = `/game`;
        this.props.history.push(path);
    }

    backButtonFun() {
        localStorage.removeItem("game");
        localStorage.setItem("game", JSON.stringify(this.state.game));
        this.changeRouteGame()
    }

    readyVote() {
        this.clientRef.sendMessage("/app/game/" + this.state.game.gameId + "/voteready/" + localStorage.getItem("username"));
    }

    componentWillMount() {
        let mapToAdd = {};
        let votedToAdd = {};
        this.state.game.players.forEach((player) => {
            mapToAdd[player] = 0;
            votedToAdd[player] = "";
        });

        this.setState({voted: votedToAdd});
        this.setState({votes: mapToAdd});
        this.loadData();
    }

    loadData() {
        let role;
        if (this.state.role === 'Role was not found, playername was invalid') {
            this.setState({
                allowedNoVote: true,
                placeholder: "You are dead."
            })
        } else if (this.state.game.day) {
            role = 'citizen';
            this.setState({chatRole: "citizen"});
        } else if (this.state.role === "citizen") {
            this.setState({chatRole: "citizen"});
            this.handleCitizenNight();
        } else {
            if (this.state.role === "maffia") {
                this.setState({chatRole: "maffia"});
            } else {
                this.setState({chatRole: "citizen"});
            }
            role = this.state.role;
        }
        fetch('https://ip2-maffia.herokuapp.com/getvotes/' + this.state.game.gameId + "/" + role, {

            method: 'POST',
            headers: new Headers({
                'Authorization': 'Bearer ' + localStorage.getItem('JwtToken'),
            })
        }).then(response => {
            if (response.status === 200) {
                response.json().then(res => {
                    res.forEach(object => this.handleMessage(object))
                });
            }
        })
    }

    handleCitizenNight() {
        this.setState({
            allowedNoVote: true,
            placeholder: "You cannot vote. you are asleep. You hope the night will be kind to you."
        });
    }

    handleClick(playerName) {
        this.clientRef.sendMessage("/app/game/" + this.state.game.gameId + "/vote", '{"voter": "' + localStorage.getItem("username") + '","voted": [ "' + playerName + '" ]}');
    }

    renderMultiplePlayers() {
        let test = [];
        for (let i = 0; i < this.state.game.players.length; i++) {
            test.push(<div onClick={this.handleClick.bind(this, this.state.game.players[i])}>
                <PlayerVote onClicked={this.handleClick.bind(this, this.state.game.players[i])}
                            name={this.state.game.players[i]} count={this.state.votes[this.state.game.players[i]]}/>
            </div>)
        }
        return test;

    }

    readyCounter(message) {
        let count = this.state.game.players.length - message;
        this.setState({readyCount: count});
    }


    powerHandle(message) {
        localStorage.setItem("power", message);
    }

    handleMessage(message) {
        /**
         * @param {{voter :string}} message
         */
        //check if the player who cast the vote has already voted
        if (this.state.voters[message.voter] !== "") {
            let votes = this.state.votes;
            //remove the previous vote from the voter(player)
            votes[this.state.voters[message.voter]] -= 1;
            //add the new vote from the voter(player)
            votes[message.voted[0]] += 1;
            //setState to render the new votes
            this.setState({votes: votes});
            let voters = this.state.voters;
            //add the voters(players) their votes to the list
            voters[message.voter] = message.voted[0];
            //setState to keep the voters in memory
            this.setState({voters: voters});
        } else {
            let voters = this.state.voters;
            //add the voters(players) their votes to the list
            voters[message.voter] = message.voted[0];
            //setState to keep the voters in memory
            this.setState({voters: voters});
            let votes = this.state.votes;
            //add the new vote from the voter(player)
            votes[message.voted[0]] += 1;
            //setState to render the new votes
            this.setState({votes: votes});
        }
    }

    render() {
        let self = this;
        return (
            <div className="voting-container row">
                <div className="voting-flexbox col-md-9">
                    <div className="col-md-12 row">
                        {this.state.allowedNoVote ?
                            <div className="no-vote">
                                <h1>{this.state.placeholder}</h1>
                            </div>
                            : this.renderMultiplePlayers()
                        }
                    </div>
                    <div className="col-md-12 readyText row">
                        <span
                            className="vote-txt">Players Ready: {this.state.readyCount} / {this.state.game.players.length}</span>
                    </div>
                    <div className="col-md-12 row">
                        <button className="btn-dark btn-vote" onClick={this.backButtonFun}>Back</button>
                        {this.state.allowedNoVote ? "" :
                            <button className="btn-dark btn-vote" onClick={this.readyVote}>Vote is ready</button>}
                    </div>
                </div>
                <div className="col-md-3">
                    {this.state.allowedNoVote ? "" : <Chat id={this.state.game.gameId + "-" + this.state.chatRole}/> }
                </div>
                <div>
                    <SockJsClient url={this.state.url}
                                  topics={["/topic/" + this.state.game.gameId + "/" + this.state.role,
                                      "/topic/" + this.state.game.gameId + "/citizen", "/topic/game/" + this.state.game.gameId + "/voteready", "/topic/game", "/topic/game/" + this.state.role]}
                                  ref={(client) => {
                                      self.clientRef = client
                                  }}
                                  onMessage={function (message, topic) {
                                      if (topic === "/topic/game/" + self.state.game.gameId + "/voteready") {
                                          self.readyCounter(message.body);
                                      } else if (topic === "/topic/game") {
                                          localStorage.removeItem("game");
                                          localStorage.setItem("game", JSON.stringify(message.body));
                                          self.changeRouteGame()
                                      } else if (topic === "/topic/game/" + self.state.role) {
                                          self.powerHandle(message.body);
                                      } else {
                                          self.handleMessage(message.body)
                                      }
                                  }}
                    />
                </div>
            </div>
        );
    }

}