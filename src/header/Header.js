import React from 'react';
import './Header.css';
import history from './history'
import {JwtHandler} from "../jwt/JwtHandler";

export class Header extends React.Component {
    constructor(props) {
        super(props);
        this.routeChangeHome = this.routeChangeHome.bind(this);
        this.jwtHandler = new JwtHandler(this);
    }

    routeChangeHome() {
            let path = `/web-client`;
            history.push(path);
            window.location.reload()
    }

    render() {
        return (
            <header className='app-header'>
                <div className="header-bar">
                    <img onClick={this.routeChangeHome} id="maffia-logo"
                         src={require('../Resources/Images/maffia_logo2.png')}
                         alt="Maffia Logo"/>
                    Maffia
                </div>

            </header>
        );
    }
}