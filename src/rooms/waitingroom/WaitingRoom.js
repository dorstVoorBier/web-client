import React from 'react';
import './WaitingRoom.css'
import SockJsClient from "react-stomp";
import {Redirect} from "react-router-dom";
import {Chat} from "../../chat/Chat";

export class WaitingRoom extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            id: this.props.location.state.id,
            roomdata: null,
            loaded: false,
            redirectBackToRooms: false,
            url: 'https://ip2-maffia.herokuapp.com/game?token=' + localStorage.getItem('JwtToken')
        };
        this.handleLeaveRoom = this.handleLeaveRoom.bind(this);
        this.renderRedirect = this.renderRedirect.bind(this);
        this.checkIfJoined = this.checkIfJoined.bind(this);
        this.routeChangeGame = this.routeChangeGame.bind(this);
    }

    routeChangeGame() {
        let path = `/game`;
        this.props.history.push(path);
    }

    render() {
        let self = this;
        if (!this.state.id) {

            return <div/>
        }
        return <div id="waitingroomrow" className="row">
        <div id="waitingroom" className="col-md-6">
            {this.renderRedirect()}
            <SockJsClient url={this.state.url}
                          topics={['/topic/' + this.state.id + '/roominfo', '/topic/game/' + self.state.id]}
                          ref={(client) => {
                              self.clientRef = client
                          }}
                          onMessage={function (message, topic) {
                              if (topic === '/topic/' + self.state.id + '/roominfo') {
                                  self.setState({roomdata: message.body, loaded: true})
                              } else if (topic === '/topic/game/' + self.state.id) {
                                  self.handleMessage(message)
                              }
                          }
                          }
                          onConnect={(frame) => {
                              self.clientRef.sendMessage("/app/" + this.state.id + "/join", JSON.stringify(localStorage.getItem('username')));
                          }}
                          onDisconnect={(frame) => {
                              fetch('https://ip2-maffia.herokuapp.com/rooms/playerdisconnects?roomid='+this.state.id+'&username='+localStorage.getItem('username'), {
                                  method: 'POST',
                                  headers: new Headers({
                                      'Authorization': 'Bearer ' + localStorage.getItem('JwtToken'),
                                  })
                              })

                          }}
            />
            {this.loadRoomData()}
            {this.checkIfJoined()}
            </div>
            <div className="col-md-4"><Chat id={this.state.id}/></div>

            </div>


    }

    handleMessage(message) {
        localStorage.removeItem("game");
        localStorage.setItem('game', JSON.stringify(message.body));
        this.routeChangeGame();
    }

    loadRoomData() {
        if (this.state.loaded) {
            return <div>
                <div className="row image-container">
                    <img id='logo-maffia' src={require('../../Resources/Images/maffia_logo2.png')} alt='logo'
                         className='header-icon'/>
                </div>
                <div className="header-rooms">{this.state.roomdata.roomName}</div>
                <div className="div-roomtext">Maximum players:</div>
                <div className="div-roomtext">{this.state.roomdata.maxSpelers}</div>
                <div className="div-roomtext">Minimum players:</div>
                <div className="div-roomtext">{this.state.roomdata.minSpelers}</div>
                <div className="div-roomtext">Time during the day:</div>
                <div className="div-roomtext">{this.state.roomdata.secDayRoundTime}</div>
                <div className="div-roomtext">Time during the night:</div>
                <div className="div-roomtext">{this.state.roomdata.secNightRoundTime}</div>
                <div className="div-roomtext">Player list:</div>

                <ul>
                    {this.state.roomdata.players.map(function (name, index) {
                        return <li className="div-roomtext" key={index}>{name}</li>;
                    })
                    }
                </ul>
                <button className="button-leave" onClick={this.handleLeaveRoom}>Leave room</button>
            </div>
        }
    }

    handleLeaveRoom() {
        this.clientRef.sendMessage("/app/" + this.state.id + "/leave", JSON.stringify(localStorage.getItem('username')) );
        this.clientRef.disconnect();
        this.setState({redirectBackToRooms: true});
    }

    renderRedirect() {
        if (this.state.redirectBackToRooms)
            return <Redirect to={{pathname: '/rooms'}}/>
    }

    checkIfJoined() {
        if (this.state.loaded) {
            let inRoom = false;
            this.state.roomdata.players.map(function (name, index) {
                if (name === localStorage.getItem('username')) {
                    inRoom = true
                }
                return null;
            });

            if (!inRoom) {
                return <Redirect to={{pathname: '/rooms'}}/>
            }
        }
    }
}