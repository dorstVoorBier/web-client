import React from 'react';
import './WaitingRoom.css'
import SockJsClient from "react-stomp";
import {Chat} from "../../chat/Chat";


export class WaitingRoomHost extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            id: this.props.location.state.id,
            roomdata: null,
            loaded: false,
            maxValidationError: "",
            minValidationError: "",
            dayValidationError: "",
            nightValidationError: "",
            url: 'https://ip2-maffia.herokuapp.com/game?token=' + localStorage.getItem('JwtToken')
        };
        this.handleChange = this.handleChange.bind(this);
        this.routeChangeHomeLogin = this.routeChangeHomeLogin.bind(this);
        this.handleLeave = this.handleLeave.bind(this);
        this.routeChangeGame = this.routeChangeGame.bind(this);
        this.startGame = this.startGame.bind(this);
    }

    routeChangeHomeLogin() {
        let path = `/home_login`;
        this.props.history.push(path);
    }

    routeChangeGame() {
        let path = `/game`;
        this.props.history.push(path);
    }

    render() {
        let self = this;
        if (!this.state.id) {
            return <div/>
        }
        return <div className="row" id="waitingroomrow">
            <div id="waitingroom" className="col-sm-6">
                <SockJsClient url={this.state.url}
                              topics={['/topic/' + self.state.id + '/roominfo', '/topic/game/' + self.state.id]}
                              onMessage={function (message, topic) {
                                  if (topic === '/topic/' + self.state.id + '/roominfo') {
                                      self.updateAndValidateForm(message)
                                  } else if (topic === '/topic/game/' + self.state.id) {
                                      self.handleMessage(message)
                                  }
                              }
                              }
                              ref={(client) => {
                                  this.clientRef = client
                              }}
                              onConnect={(frame) => {
                                  self.clientRef.sendMessage("/app/" + this.state.id + "/join", JSON.stringify(localStorage.getItem('username')));
                              }}
                              onDisconnect={(frame) => {
                                  fetch('https://ip2-maffia.herokuapp.com/rooms/playerdisconnects?roomid=' + this.state.id + '&username=' + localStorage.getItem('username'), {
                                      method: 'POST',
                                      headers: new Headers({
                                          'Authorization': 'Bearer ' + localStorage.getItem('JwtToken'),
                                      })
                                  })

                              }}
                />

                {this.loadroomdata()}


            </div>
            <div className="col-sm-4"><Chat id={this.state.id}/></div>
        </div>
    }

    startGame() {
        this.clientRef.sendMessage("/app/startgame/" + this.state.id);
    }

    handleMessage(message) {
        localStorage.removeItem("game");
        localStorage.setItem("game", JSON.stringify(message.body));
        this.routeChangeGame();
    }

    loadroomdata() {
        let self = this;
        if (this.state.loaded) {
            return <div>
                <div className="container-rooms">
                    <div className="row image-container">
                        <img id='logo-maffia' src={require('../../Resources/Images/maffia_logo2.png')} alt='logo'
                             className='header-icon'/>
                    </div>
                    <div className="header-rooms">{this.state.roomdata.roomName}</div>
                    <form onSubmit={this.handleSubmit}>
                        <div className="row row-container">
                            <div className="div-roomtext col-md-4">
                                Maximum players:
                            </div>
                            <input className="input-rooms col-md-7" type="text" value={this.state.roomdata.maxSpelers}
                                   onChange={this.handleChange}
                                   id="max"/>
                        </div>
                        {self.showMaxValidation()}
                        <div className="row row-container">
                            <div className="div-roomtext col-md-4">
                                Minimum players:
                            </div>
                            <input className="input-rooms col-md-7" type="text" value={this.state.roomdata.minSpelers}
                                   onChange={this.handleChange}
                                   id="min"/>
                        </div>
                        {self.showMinValidation()}
                        <div className="row row-container">
                            <div className="div-roomtext col-md-4">
                                Duration of the day:
                            </div>
                            <input className="input-rooms col-md-7" type="text"
                                   value={this.state.roomdata.secDayRoundTime}
                                   onChange={this.handleChange}
                                   id="day"/>
                        </div>
                        {self.showDayValidation()}
                        <div className="row row-container">
                            <div className="div-roomtext col-md-4">
                                Duration of the night:
                            </div>
                            <input className="input-rooms col-md-7" type="text"
                                   value={this.state.roomdata.secNightRoundTime}
                                   onChange={this.handleChange} id="night"/>

                        </div>
                        {self.showNightValidation()}
                    </form>
                    <div className="div-roomtext">List of players</div>

                    <ul>
                        {this.state.roomdata.players.map(function (name, index) {
                            return <li className="div-roomtext" key={index}>{name}</li>;
                        })
                        }
                    </ul>
                </div>

                <div className="row col-md-6">
                    {self.showStartButton()}
                </div>
                <div className="row col-md-6">
                    <button className="back_button" onClick={this.handleLeave}>Back</button>
                </div>
            </div>
        }
    }

    handleChange(event) {
        this.clientRef.sendMessage("/app/" + this.state.id + "/settingchange", JSON.stringify({
            "setting": event.target.id,
            "value": event.target.value
        }))
    }


    showStartButton() {
        let self = this;
        if (this.state.minValidationError === "" && this.state.maxValidationError === "" && this.state.dayValidationError === "" && this.state.nightValidationError === "") {
            if (this.state.roomdata.players.length > this.state.roomdata.minSpelers || this.state.roomdata.players.length === this.state.roomdata.minSpelers) {
                return (<input className="back_button" type="submit" value="Start" onClick={function () {
                    self.clientRef.sendMessage("/app/startgame/" + self.state.id)}
                }/>)
            }
        }
    }


    showMinValidation() {
        if (this.state.minValidationError !== "") {
            return <div className="row row-container">
                <div className="div-roomtext col-md-4">{this.state.minValidationError}</div>
            </div>
        }
    }

    showMaxValidation() {
        if (this.state.maxValidationError !== "") {
            return <div className="row row-container">
                <div className="div-roomtext col-md-4">{this.state.maxValidationError}</div>
            </div>
        }
    }

    showDayValidation() {
        if (this.state.dayValidationError !== "") {
            return <div className="row row-container">
                <div className="div-roomtext col-md-4">{this.state.dayValidationError}</div>
            </div>
        }
    }

    showNightValidation() {
        if (this.state.nightValidationError !== "") {
            return <div className="row row-container">
                <div className="div-roomtext col-md-4">{this.state.nightValidationError}</div>
            </div>
        }
    }

    updateAndValidateForm(message) {

        this.setState({roomdata: message.body, loaded: true});
        //validate max players field
        if (this.state.roomdata.maxSpelers < 7) {
            this.setState({maxValidationError: "The minimum allowed players is 7"})
        } else if (this.state.roomdata.maxSpelers > 20) {
            this.setState({maxValidationError: "The maximum allowed players is 20"})
        } else if (this.state.roomdata.maxSpelers < this.state.roomdata.minSpelers) {
            this.setState({maxValidationError: "The maximum amount of players can't be less then the minimum amount of players"})
        } else this.setState({maxValidationError: ""});

        //validate min players field
        if (this.state.roomdata.minSpelers < 7) {
            this.setState({minValidationError: "The minimum allowed players is 7"})
        } else if (this.state.roomdata.minSpelers > 20) {
            this.setState({minValidationError: "The maximum allowed players is 20"})
        } else if (this.state.roomdata.minSpelers > this.state.roomdata.maxSpelers) {
            this.setState({minValidationError: "The minimum amount of players can't be more then the maximum amount of players"})
        } else this.setState({minValidationError: ""});
        //validate sec day round time field
        if (this.state.roomdata.secDayRoundTime < 1) {
            this.setState({dayValidationError: "The duration of the day has to be positive"})
        } else this.setState({dayValidationError: ""});
        //validate sec night round time
        if (this.state.roomdata.secNightRoundTime < 1) {
            this.setState({nightValidationError: "The duration of the night has to be positive"})
        } else this.setState({nightValidationError: ""});

        this.changeZeroToNothing(message);
    }

    changeZeroToNothing(message) {
        let roomdatalet = Object.assign({}, this.state.roomdata)
        if (message.body.minSpelers === 0) {
            roomdatalet.minSpelers = ""

        }
        if (message.body.maxSpelers === 0) {
            roomdatalet.maxSpelers = ""
        }
        if (message.body.secDayRoundTime === 0) {
            roomdatalet.secDayRoundTime = ""
        }
        if (message.body.secNightRoundTime === 0) {
            roomdatalet.secNightRoundTime = ""
        }
        this.setState({roomdata: roomdatalet})

    }

    handleLeave() {
        let self = this;
        //disconnect all the players from the room
        self.clientRef.sendMessage("/app/" + self.state.id + "/leave", JSON.stringify(localStorage.getItem('username')));
        self.clientRef.disconnect();
        self.routeChangeHomeLogin();
    }

}



