import React from "react";
import './CreateRoomButton.css'
import {Redirect} from "react-router-dom";


export class CreateRoomButton extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: null,
            redirect: false
        };
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    render() {
        return (
            <div>
                {this.renderRedirect()}
                <button id="createroom-button" className="startButton" onClick={this.props.handleClick}>Create a
                    room
                </button>
            </div>
        );
    }

    renderRedirect = () => {
        if (this.state.redirect) {
            return <Redirect to={{pathname: '/waitingroomhost', state: {id: this.state.data.id}}}/>
        }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if(this.props.canSubmit && !this.state.redirect){
            this.handleSubmit();
        }
    }

    handleSubmit() {

        if (this.props.roomName !== null && this.props.roomName !== "") {
            let self = this;
            fetch('https://ip2-maffia.herokuapp.com/rooms/create', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + localStorage.getItem('JwtToken'),
                },
                body: JSON.stringify({
                    roomName: this.props.roomName,
                    maxSpelers: "20",
                    minSpelers: "7",
                    secDayRoundTime: "600",
                    secNightRoundTime: "300",
                    password: "",
                    host: localStorage.getItem('username'),
                    full: "false",
                    closed: "false"
                })
            }).then(response => {
                if (response.ok) {
                    return response.json();
                }
            }).then(function (jsonData) {
                self.setState({data: jsonData, redirect: true});
            });
        }
    }
}
