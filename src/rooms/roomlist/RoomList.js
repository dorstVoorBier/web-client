import React from 'react';
import './RoomList.css'
import {RoomItem} from "../roomitem/RoomItem";

export class RoomList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: null,
            showing: true,
        };
        this.componentDidMount = this.componentDidMount.bind(this);
        this.changeRouteBackHome = this.changeRouteBackHome.bind(this)
    }

    changeRouteBackHome() {
        let path = `/home_login`;
        this.props.history.push(path);
    }

    componentWillMount() {
        this.loadData();
    }

    loadData() {
        fetch('https://ip2-maffia.herokuapp.com/rooms/getrooms', {
            method: 'GET',
            headers: new Headers({
                'Authorization': 'Bearer ' + localStorage.getItem('JwtToken'),
            })
        }).then(response => response.json()).then(res => this.setState({data: res}))
    }

    componentDidMount() {
        window.addEventListener("resize", () => {
            if (window.outerWidth < 1100) {
                this.setState({showing: false})
            } else if (window.outerWidth >= 1100) {
                this.setState({showing: true})
            }
        });
    }

    render() {
        return (
                <div className="container-roomlist">
                    <ul>
                        <RoomItem roomname="Room name" players="Player Count" roomsize="Room Size" showing={this.state.showing}/>
                        {this.state.data ? this.state.data.map((data, index) => {
                            return <RoomItem key={index} roomname={data.roomName} players={data.aantalSpelers}
                                             roomsize={data.maxAantalSpelers} id={data.roomId} joinable="true" showing="true" />
                        }) : null}
                    </ul>
                    <div className="row col-md-6">
                        <button className="button-back" onClick={this.changeRouteBackHome}>Back</button>
                    </div>
                </div>
        );
    }
}

