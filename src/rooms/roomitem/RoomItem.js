import React from 'react';
import './RoomItem.css'


import {Redirect} from 'react-router-dom';

export class RoomItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            roomname: this.props.roomname,
            players: this.props.players,
            roomsize: this.props.roomsize,
            id: this.props.id,
            redirect: false,
            joinable: this.props.joinable
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.showJoinButton = this.showJoinButton.bind(this);
        this.showHeaderRow = this.showHeaderRow.bind(this);
    }


    render() {
        return (
            <div>
                {this.showHeaderRow()}
            </div>
        );
    }

    renderRedirect = () => {
        if (this.state.redirect) {
            return <Redirect to={{pathname: '/waitingroom', state: {id: this.state.id}}}/>
        }
    };

    handleSubmit() {
        this.setState({redirect:true})
    }

    showJoinButton = () => {
        if (this.state.joinable) {
            return <div className="col-md-2">
                <button className="join-button" id="join" onClick={this.handleSubmit}>Join</button>
            </div>
        }
    };

    showHeaderRow() {
        if (this.props.showing) {
            return <div id='Room'>
                {this.renderRedirect()}
                <div className="row room-list">
                    <div className="col-md-6 col-container">{this.state.roomname}</div>
                    <div className="col-md-2 col-container">{this.state.players} </div>
                    <div className="col-md-2 col-container">{this.state.roomsize} </div>
                    {this.showJoinButton()}
                </div>
            </div>
        }
    }
}