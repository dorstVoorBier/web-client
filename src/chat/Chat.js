import React from 'react';
import './Chat.css';
import SockJsClient from 'react-stomp';
import {JwtHandler} from "../jwt/JwtHandler";

export class Chat extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            messages: [],
            input: "",
            stompClient: null,
            url: 'https://ip2-maffia.herokuapp.com/game?token='+localStorage.getItem('JwtToken'),
            id: this.props.id
        };
        //this.connect = this.connect.bind(this);
        this.sendMessage = this.sendMessage.bind(this);
        this.jwtHandler = new JwtHandler(this);
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        this.triggerScroll();
    }

    triggerScroll () {
        this.cont.scrollIntoView({behavior:'smooth'});
    }

    sendMessage() {
        this.clientRef.sendMessage("/app/" + this.state.id + "/allChat",
            JSON.stringify({
                'content': this.state.input,
                'playerName': localStorage.getItem('username')
            }));
        this.setState({input:""})
    }

    /**
     * updates the messages displayed in the chatroom
     * @param message
     */
    handleMessage(message){
        let mess = message.playerName + ": " + message.content;
        let newArray = this.state.messages.slice();
        newArray.push(mess);
        this.setState({messages: newArray});
    }

    render() {

        let chat = this.state.messages.map((message, index) =>
            <div key={index} className="flex-item-chat">{message}</div>
        );

        return(
            <div id='Chat'>
                { this.state.id ?
                <SockJsClient url={this.state.url} topics={['/topic/chat/' + this.state.id]}
                              ref={(client) => {this.clientRef = client}}
                              onMessage={(message) => {this.handleMessage(message)}}

                /> : null}
                <div id='chat-title'>Chatroom</div>
                <div id="chat-window" className="flex-container-chat">
                    {chat}
                    <div ref={(node) => {this.cont = node}}> </div>
                </div>
                <div id="chat-input-box">
                    <div className="button-container" >
                        <button id="chat-input-send"  onClick={this.sendMessage}>Send</button>
                    </div>
                    <div className="input-container">
                        <input id="chat-input" type="text" onKeyPress={event => {
                            if (event.key === "Enter") {
                                this.sendMessage();
                            }
                        }} placeholder="your message..." value={this.state.input} onInput={(e) => {this.setState({input: e.target.value})}}/>
                    </div>
                </div>
            </div>
        )
    }
}