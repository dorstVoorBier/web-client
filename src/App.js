import React, {Component} from 'react';
import './App.css';
import {BrowserRouter, Switch} from 'react-router-dom';
import Modal from 'react-responsive-modal';
import {Route} from 'react-router-dom';
import {Login} from './login/Login.js';
import {Register} from "./register/Register";
import {GameScreen} from "./gameScreen/GameScreen";
import {Settings} from "./settings/Settings";
import {Home_logged_in} from "./home_logged_in/Home_logged_in";
import {Home} from "./home/Home";
import {CreateRoom} from "./createRoom/CreateRoom";
import {VotingScreen} from "./votingScreen/VotingScreen";
import {Header} from "./header/Header";
import {RoomList} from "./rooms/roomlist/RoomList";
import {WaitingRoom} from "./rooms/waitingroom/WaitingRoom";
import {WaitingRoomHost} from "./rooms/waitingroom/WaitingRoomHost";
import {RoomItem} from "./rooms/roomitem/RoomItem";
import {EndScreen} from "./gameScreen/EndScreen";
import {Profile} from "./profile/Profile";


class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            open: false,
            rulesText: ""
        };

        this.onOpenModal = this.onOpenModal.bind(this);
        this.onCloseModal = this.onCloseModal.bind(this);
    }

    componentWillMount() {
        fetch("https://ip2-maffia.herokuapp.com/rules",
            {
                method: 'GET',
                headers: new Headers({
                    'Authorization': 'Bearer ' + localStorage.getItem('JwtToken'),
                })
            }).then(response => {
            if (response.status === 200) {
                response.json().then(res => {
                    this.setState({rulesText: res.data})
                });
            }
        })
    }

    onOpenModal() {
        this.setState({open: true});
    }

    onCloseModal() {
        this.setState({open: false});
    }

    render() {
        return (
            <div className="wrapper">
                <Header/>
                <BrowserRouter basename="/web-client">
                    <Switch>
                        <Route exact path='/login' component={Login}/>
                        <Route exact path='/' component={Home}/>
                        <Route exact path='/register' component={Register}/>
                        <Route exact path='/game' component={GameScreen}/>
                        <Route exact path='/home_login' component={Home_logged_in}/>
                        <Route exact path='/settings' component={Settings}/>
                        <Route exact path='/createroom' component={CreateRoom}/>
                        <Route exact path='/vote' component={VotingScreen}/>
                        <Route exact path='/roomitem' component={RoomItem}/>
                        <Route exact path='/rooms' component={RoomList}/>
                        <Route exact path='/waitingroom' component={WaitingRoom}/>
                        <Route exact path='/waitingroomhost' component={WaitingRoomHost}/>
                        <Route exact path='/endScreen' component={EndScreen}/>
                        <Route exact path='/profile' component={Profile}/>
                    </Switch>
                </BrowserRouter>
                <Modal onClose={this.onCloseModal} open={this.state.open} classNames={{modal: "modal-style"}}>
                    <p className="rules-text">
                        {this.state.rulesText}
                    </p>
                </Modal>
                <footer className='app-footer'>
                    <p> © 2019 Team DorstVoorBier All Rights Reserved</p>
                    <a href="todo"><p>Terms of service</p></a>
                    <button className="footer-button" onClick={this.onOpenModal}><p>Rules</p></button>
                </footer>
            </div>
        );
    }
}

export default App;
