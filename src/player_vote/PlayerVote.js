import React from 'react';
import './PlayerVote.css'
import {Card, CardImg, CardText, CardTitle} from "reactstrap";

export class PlayerVote extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: this.props.name,
        };
    }

    render() {
            return (
                <div>
                    <Card className="player" body
                    >
                        <CardTitle>{this.state.name}</CardTitle>
                        <CardImg top width="100%"
                                 src={require('../Resources/Images/cards/89848-1532336916.jpg')} alt="card-front"
                                 id="card-image"/>

                        <CardText>Votes: {this.props.count}</CardText>
                    </Card>
                </div>
            );
    }
}