import React from 'react';
import './PlayerWin.css'
import {Card, CardImg, CardText, CardTitle} from "reactstrap";

export class PlayerWin extends React.Component {
    render() {
        return (
            <div className="player-win">
                <Card className="winner" body>
                    <CardTitle>{this.props.name}</CardTitle>
                    <CardImg top width="100%"
                             src={require('../Resources/Images/cards/89848-1532336916.jpg')} alt="card-front"
                             id="card-image"/>
                    <CardText>{this.props.playerRole.charAt(0).toUpperCase() + this.props.playerRole.slice(1)}</CardText>
                    <CardText>{this.props.status}</CardText>
                </Card>
            </div>
        );
    }
}