import React from 'react';
import './CreateRoom.css'

export class CreateRoom extends React.Component {

    render() {
        return (
            <div id='createroom' className='createroom-box'>
                <div className='createroom-header'>
                    <img id='createroom-logo' src={require('../Resources/Images/maffia_logo2.png')} alt='logo' className='header-icon'/>
                </div>
                <div className='createroom'>
                    <input className="createroom-input" placeholder='Room name' type='text' id='Roomname' onInput={e => this.setState({roomname: e.target.value})} />
                    <input className="createroom-input" placeholder='Password' type='password' id='password' onInput={e => this.setState({password: e.target.value})} />
                    <input className="createroom-input" placeholder='Max ammount of players' type='text' id='maxplayers' onInput={e => this.setState({maxplayers: e.target.value})} />
                    <input className="createroom-input" placeholder='Length night' type='text' id='lengthnight' onInput={e => this.setState({lengthnight: e.target.value})} />
                    <input className="createroom-input" placeholder='Length day' type='text' id='lengthday' onInput={e => this.setState({lengthday: e.target.value})} />
                    <input type='button' id='create' value='Create Room' className='btn' onClick={this.handleSubmit} />

                </div>
            </div>
        );
    }
}