import React from 'react';
import './Home.css'

export class Home extends React.Component {
    constructor(props) {
        super(props);
        this.routeChangeRegister = this.routeChangeRegister.bind(this);
        this.routeChangeLogin = this.routeChangeLogin.bind(this);
    }
    routeChangeRegister(){
        let path = `/register`;
        this.props.history.push(path);
    }
    routeChangeLogin(){
        let path = `/login`;
        this.props.history.push(path);
    }

    render() {
        return (
            <div className="startpage">
                <div className='startpage-header'>
                    <img id='startpage-logo' src={require('../Resources/Images/maffia_logo2.png')} alt='logo' className='startpage-logo'/>
                </div>
                <div className="startButtons">
                    <button id="login-button" className="startButton" onClick={this.routeChangeLogin}>Log In</button>
                    <button className="startButton" onClick={this.routeChangeRegister}>Register</button>
                </div>

            </div>
        );
    }
}
