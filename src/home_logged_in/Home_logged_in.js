import React from 'react';
import './Home_logged_in.css'
import {CreateRoomButton} from "../rooms/createroom/CreateRoomButton";
import {JwtHandler} from '../jwt/JwtHandler';

export class Home_logged_in extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            createRoom: false,
            roomName: "",
            hidden: "hidden"
        };
        this.routeChangeContinue = this.routeChangeContinue.bind(this);
        this.routeChangeCreate = this.routeChangeCreate.bind(this);
        this.routeChangeSettings = this.routeChangeSettings.bind(this);
        this.routeChangeRooms = this.routeChangeRooms.bind(this);
        this.routeChangeProfile = this.routeChangeProfile.bind(this);
        this.handleClick = this.handleClick.bind(this);
        this.handleCreateClick = this.handleCreateClick.bind(this);
        this.routeChangeRooms = this.routeChangeRooms.bind(this);
        this.logOutOnClick = this.logOutOnClick.bind(this);
        this.jwtHandler = new JwtHandler(this);
    }

    componentWillMount() {
        if(this.jwtHandler.isTokenExpired(this.jwtHandler.getToken()) || this.jwtHandler.getToken() === null){
            this.props.history.push("/");
        }
    }

    logOutOnClick() {
        localStorage.clear();
        let path = `/`;
        this.props.history.push(path);
    }

    routeChangeRooms() {
        let path = `/rooms`;
        this.props.history.push(path);
    }

    routeChangeContinue() {
        let path = `/todo`;
        this.props.history.push(path);
    }

    routeChangeCreate() {
        let path = `/rooms`;
        this.props.history.push(path);
    }

    routeChangeSettings() {
        let path = `/settings`;
        this.props.history.push(path);
    }
    routeChangeProfile(){
        let path = `/profile`;
        this.props.history.push(path);
    }

    handleClick() {
        this.setState({hidden: ""});
    }

    handleCreateClick() {
        this.setState({createRoom: true});
    }

    render() {
        return (
            <div>
                <div className="startpage">
                        <div className='startpage-header'>
                            <img id='startpage-logo' src={require('../Resources/Images/maffia_logo2.png')} alt='logo'
                                 className='startpage-logo'/>
                        </div>
                        <div className="startButtons">
                            <CreateRoomButton handleClick={this.handleClick}
                                              canSubmit={this.state.createRoom}
                                              roomName={this.state.roomName}
                            />
                            <button className="startButton" onClick={this.routeChangeRooms}>Join room</button>
                        </div>
                        <div className="create-inputs" hidden={this.state.hidden}>
                            <input type="text" id="roomname-text"
                                   onInput={e => this.setState({roomName: e.target.value})}
                                   className="createroom-text"
                            />
                            <input id="roomname-button" type="button" value="create" className="createroom-button btn-dark"
                                   onClick={this.handleCreateClick}
                            />

                        </div>
                        <div className="startButtons">
                            <button className="startButton" onClick={this.routeChangeSettings}>Settings</button>
                            <button className="startButton" onClick={this.routeChangeProfile}>Profile</button>
                        </div>

                    <div className="startButtons">
                    </div>

                        <div className="row logout-container">
                            <button className="btn-dark button-logout" onClick={this.logOutOnClick}>Log Out</button>
                        </div>
                    </div>
                </div>
        );
    }
}
