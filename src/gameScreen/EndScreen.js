import React from 'react';
import './EndScreen.css'

import {PlayerWin} from "../player_win/PlayerWin";

export class EndScreen extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            loading: true,
            winner: "",
            winners: []
        };
        this.loadData = this.loadData.bind(this);
    }

    componentWillMount() {
        this.loadData().then(() => {
            this.setState({loading: false});
        })
    }

    /**
     * Checks every player left alive and gets their role. then if the role matches the winner it will be added to the
     * array that contains the winning player components.
     * After that it checks the dead players and adds them if they have a matching role.
     * Finally it updates the state so the page will refresh and render the added components
     * @returns {Promise<void>}
     */
    async loadData(){
        let game = JSON.parse(localStorage.getItem('game'));
        let winner = game.winner;
        let winners = [];
        if(winner === "maffia"){
            this.setState({winner: "Maffia's"});
        } else {
            this.setState({winner: "Citizens"});
        }
        for (let i = 0; i < game.players.length; i++) {
            let name = game.players[i];
            await fetch("https://ip2-maffia.herokuapp.com/player/" + game.gameId + "/" + name,
                {
                    method: 'POST',
                    headers: new Headers({
                        'Authorization': 'Bearer ' + localStorage.getItem('JwtToken'),
                    })
                }
            ).then(async (res) => {
                await res.json().then((data) => {
                    let role = data.rolename;
                    if (winner === "maffia") {
                        if (role === winner) {
                            winners.push(<div key={winners.length} className="player-win-item"><PlayerWin status="Alive" playerRole={role}
                                                                                     name={name}/></div>);
                        }
                    } else {
                        if (role !== "maffia") {
                            winners.push(<div key={winners.length} className="player-win-item"><PlayerWin status="Alive" playerRole={role}
                                                                                     name={name}/></div>);
                        }
                    }
                });
            })
        }
        for (let player in game.deadPlayers) {
            let role = game.deadPlayers[player];
            if (winner === "maffia"){
                if (role === winner) {
                    winners.push(<div key={winners.length} className="player-win-item"><PlayerWin status="Dead" playerRole={role} name={player}/></div>);
                }
            } else if (role !== "maffia"){
                winners.push(<div key={winners.length} className="player-win-item"><PlayerWin status="Dead" playerRole={role} name={player}/></div>);
            }
        }
        await this.setState({winners: winners});
    }

    render() {
        return (
            <div id="EndScreen">
                <h1 className="winner-title">Winners: {this.state.winner}</h1>
                <div className="winner-box">
                    {this.state.loading ? <p className="loading-text">"Loading..."</p> : this.state.winners}
                </div>
            </div>
        )
    }
}