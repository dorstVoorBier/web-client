import React from 'react';
import './GameScreen.css'

import {PlayerCard} from "../player_card/PlayerCard";
import {PlayerDead} from "../player_dead/PlayerDead";
import SockJsClient from "react-stomp";
import {Chat} from "../chat/Chat";
import Day from "../Resources/Images/day.jpg";
import Night from "../Resources/Images/night.jpg";
import Background from '../Resources/Images/background.jpg';

export class GameScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            playerRole: null,
            game: JSON.parse(localStorage.getItem('game')),
            url: 'https://ip2-maffia.herokuapp.com/game?token=' + localStorage.getItem('JwtToken'),
            buttonText: "Show Users",
            dayNight: null,
            canChat: false,
            dead: ""
        };
        this.loadData = this.loadData.bind(this);
        this.changeRouteVote = this.changeRouteVote.bind(this);
        this.renderPlayerList = this.renderPlayerList.bind(this);
        this.showPlayerList = this.showPlayerList.bind(this);
        this.checkDayNight = this.checkDayNight.bind(this);
        this.handleMessage = this.handleMessage.bind(this);
        this.renderDeadPlayers = this.renderDeadPlayers.bind(this);
        this.componentDidMount = this.componentDidMount.bind(this);
    }

    changeRouteVote() {
        localStorage.removeItem("game");
        localStorage.setItem("game", JSON.stringify(this.state.game));
        localStorage.setItem("role", this.state.playerRole);
        let path = `/vote`;
        this.props.history.push(path);
    }

    renderDeadPlayers() {
        let deadPlayerList = [];
        for (let name in this.state.game.deadPlayers) {
            if (this.state.game.deadPlayers.hasOwnProperty(name)) {
                let role = this.state.game.deadPlayers[name];
                deadPlayerList.push(<PlayerDead name={name} role={role} gameId={this.state.game.gameId}/>)
            }
        }
        return deadPlayerList;
    }

    componentWillMount() {
        this.loadData();
        this.checkDayNight();
        this.checkEnd();
    }

    componentDidMount() {
        let power = localStorage.getItem("power");
        if (power !== null && power !== undefined) {
            alert(power);
            localStorage.removeItem("power");
        }
    }

    checkEnd() {
        if (this.state.game.winner !== null) {
            document.body.style.background = 'url(' + Background + ') no-repeat center center fixed';
            this.props.history.push("/endscreen");
        }
    }

    loadData() {
        fetch('https://ip2-maffia.herokuapp.com/player/' + this.state.game.gameId + '/' + localStorage.getItem("username"), {

            method: 'POST',
            headers: new Headers({
                'Authorization': 'Bearer ' + localStorage.getItem('JwtToken'),
            })
        }).then(response => {
            if (response.status === 200) {
                response.json().then(res => {
                    this.setState({playerRole: res.rolename})
                });
            } else {
                this.setState({playerRole: "Role was not found, playername was invalid", dead: "dead"});
            }
        })
    }

    renderPlayerList() {
        let players = [];
        for (let i = 0; i < this.state.game.players.length; i++) {
            players.push(<li className="list-group-item listitem">
                {this.state.game.players[i]}
            </li>);
        }
        return players;

    }

    showPlayerList() {
        let x = document.getElementById("playerList");
        if (x.style.display === "block") {
            this.setState({buttonText: "Show Users"})
            x.style.display = "none";
        } else {
            x.style.display = "block";
            this.setState({buttonText: "Hide Users"})
        }
    }

    /**
     * Looks if it is day or night, depending on the state it wil change the background to the corresponding one.
     * Also checks if a user can chat and sets the state accordingly
     */
    checkDayNight() {
        if (this.state.game.day) {
            this.setState({dayNight: "Day", canChat: true});
            document.body.style.background = 'url(' + Day + ') no-repeat center center fixed'
        } else {
            if(localStorage.getItem("role") === "maffia"){
                this.setState({canChat: true});
            } else {
                this.setState({canChat: false})
            }
            this.setState({dayNight: "Night"});
            document.body.style.background = 'url(' + Night + ') no-repeat center center fixed'
        }
        if(localStorage.getItem("role") === "Role was not found, playername was invalid"){
            this.setState({canChat: true, dead: "dead"});
        }
    }

    async handleMessage(message) {
        await this.setState({game: message.body});
        this.checkEnd();
        this.checkDayNight();
    }

    render() {
        let self = this;
        return (
            <div id="game">
                <div className="row container-row">
                    <div className="col-md-3 first-column-dead">
                        <div className="card-dead-container">
                            <div className="dead-flexbox col-md-12">
                                {this.renderDeadPlayers()}

                            </div>
                        </div>
                    </div>
                    <div className="col-md-6 card-div">
                        <PlayerCard playerRole={this.state.playerRole}/>
                        <div className="row">
                            <button className="btn-dark user-btn" onClick={this.changeRouteVote}>Ready to vote!
                            </button>
                            <button className="btn-dark user-btn"
                                    onClick={this.showPlayerList}>{this.state.buttonText}</button>
                        </div>
                        <div className="round-text-div">
                            <p className="round-text">It is {this.state.dayNight}Time</p>
                            <p className="round-text">Round: {this.state.game.roundCount}</p>
                        </div>

                    </div>
                    <div className="col-md-3">
                        {this.state.canChat ? <Chat id={this.state.game.gameId + this.state.dead} /> : ""}
                    </div>
                </div>
                <div className="col-md-3 column-list">
                    <div id="playerList" className="row container-list">
                        <ul className="list-group player-list">
                            {this.renderPlayerList()}
                        </ul>
                    </div>
                </div>
                <div>
                    <SockJsClient url={this.state.url}
                                  topics={["/topic/game"]}
                                  ref={(client) => {
                                      self.clientRef = client
                                  }}
                                  onMessage={function (message) {
                                      localStorage.removeItem("game");
                                      localStorage.setItem("game", JSON.stringify(message.body));
                                      self.handleMessage(message);

                                  }}
                    />
                </div>
            </div>
        );
    }
}
