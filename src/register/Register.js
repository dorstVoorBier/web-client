import React from 'react';
import './Register.css'

export class Register extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: null,
            firstname: null,
            lastname: null,
            email: null,
            password: null,
            emailValid: "hidden"
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.changeRouteBackHome = this.changeRouteBackHome.bind(this)
    }

    changeRouteBackHome() {
        let path = `/`;
        this.props.history.push(path);
    }

    async handleSubmit() {
        if(this.state.email.match(/.+@.+\..+/g) === null){
            this.setState({emailValid: ""});
        } else {
            let resp = await fetch('https://ip2-maffia.herokuapp.com/users/sign-up', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    "username": this.state.username,
                    "firstname": this.state.firstname,
                    "lastname": this.state.lastname,
                    "email": this.state.email,
                    "password": this.state.password
                })
            });
            if (resp.status === 200) {
                this.props.history.push('/login')
            } else {
                alert('Something went wrong, statuscode: ' + resp.status)
            }
        }
    }

    render() {
        return (
            <div id='register' className='register-box col-md-4'>
                <div className='register-header'>
                    <img id='register-logo' src={require('../Resources/Images/maffia_logo2.png')} alt='logo'
                         className='header-icon'/>
                </div>
                <div className='register'>
                    <h2 className='register-title'>Register</h2>
                    <input className="register-input" placeholder='Username' type='text' id='username-register'
                           onInput={e => this.setState({username: e.target.value})}/>

                    <input className="register-input" placeholder='First name' type='text' id='firstname'
                           onInput={e => this.setState({firstname: e.target.value})}/>

                    <input className="register-input" placeholder='Last name' type='text' id='lastname'
                           onInput={e => this.setState({lastname: e.target.value})}/>

                    <input className="register-input" placeholder='Email' type='text' id='email'
                           onInput={e => this.setState({email: e.target.value})}/>
                    <p className="email-error" hidden={this.state.emailValid}>Not a valid email address</p>
                    <input className="register-input" placeholder='Password' type='password' id='password'
                           onInput={e => this.setState({password: e.target.value})}/>
                    <input type='button' className='button-register' value='Sign Up' onClick={this.handleSubmit}/>
                    <input type='button' className='button-register' value='Back'
                           onClick={this.changeRouteBackHome}/>
                </div>
            </div>
        );
    }
}