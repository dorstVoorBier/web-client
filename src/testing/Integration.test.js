const puppeteer = require('puppeteer');

describe('creating a waitingroom and enter a chatmessage', () => {
    test('All necessary elements ', async () => {
        let browser = await puppeteer.launch({
            headless: false
        });
        let page = await browser.newPage();
        await page.emulate({
            viewport: {
                width: 500,
                height: 900
            },
            userAgent: ''
        });

        await page.goto('http://localhost:3000/web-client');
        await page.waitForSelector('.startpage');
        let homeHtml = await page.$eval('.startpage', e => e.innerHTML);
        await expect(homeHtml).toContain('<button id="login-button" class="startButton">Log In</button>');
        await page.click('Button[id=login-button]');
        await page.waitForSelector('.login-box');
        let loginHtml = await page.$eval('.login-box', e => e.innerHTML);
        await expect(loginHtml).toContain(
            '<form class="login"><div id="username-container"><div id="input-box"><div id="icon-container">' +
            '<img id="username-icon" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAcIAAAHCCAQAAADWEQHOAAAAAmJLR0QA/4ePzL8AAAAJcEhZcwAALiMAAC4jAXilP3YAAAAHdElNRQfjAggSCBVZ1qH+AAAZpUlEQVR42u3daXhV1b3H8d8GFAmKJgEEEgUCCIgiEpSWmRJEcUItTtepVbFabev1Iiq9BVtrcXisfUTrbKtWFJyKiANYRlFCmEGEQAIR0JBJEYPD1XVfgEAYwsk5e1h77+/nhYJCzt7/9f+dtfbZw5EAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAxE/5uNLKT01VDpUIO4cShEHZxC3DS7RBZZql71Str1Stb5SlDGWqWmmSpHxJaWqh5mqjNspSK2VNanYhtSOESMGHZoU+0CYV6tSk/n61CnS82ipX3dR2YfMeVJQQIiFrzVIVqEAr1d/Fn7pAJ6uHctRHLRlzQogDmWkK9Ko2qJ+nr7JZZ2qAOrRLL6LihBC7/Me8pCkeh6+m+RqgC3U6408I8a6ZqlfUJ5DXXqYfdJZO1yBHqsphZiSEsbPevKZXlW3BlpToCp2rFvQCIYyD0sqjMySpwDyiWfv5zHOZuga0ZYU6RVerB/1ACOPgBfOosizdtmI9oRacXSSE0fWJWaw7dKLlW7lV1+kc+oIQRtE083c1DMm2lulmnUlvEMIomWvu23lxWXh8pZs0mP7wQT1K4LXFZoR5WGmSqkO13Y11lc4zFSMYQWbCkBtrXtEJId7+mbpTI+gSQhhWH5pRahWB/SjVvZy8IIThU5Vz97qNEdqfdrrL2bFfXFnDMWEozDGDIhVBaZ16mveMlF5UPm7PtxrGmpnQSn81+ZHcrxkarZvoGGZCu1XmbTanRTSC0kDN0+VmnZHKJjLW7mlACdxTMaLssfMDu/rTH/+nSzXNNGM+ZDlqp4fMvHi82ejnnLYghDb58fPCkWZjjPb6GN1L73BMaIv0ImlLwSCzURUx2utPNMQw9oTQGmvNubnNJWXGaJ+rlaFOZr2RigkjIQzafDNQbWK312mSTtbZKjBtWZZyTBis983l6hnj/V+sl3UCXcRMGJx3zXj1lDQzthU4WRcp30illXQDM2EgEbw54mcFEzNbz+g0OokQ+m+OeYQiSCpUB83WZOXSSyxH/ZVvLqYIkqQOkrrofq03XNCdHC5bS0qxGeDr87JtlynpNG0y6cyGzIT+qBhxvnpRhr3k6gxV5lEHjgl90d+0ogi7zNSAXb/ertfpKELovZtMOUXYr2qlKVPj6SmWo16qzHvAlO88K7iRcuwlTVKFHuEyNmZC75RWFqf/jTIc1HUaQF8xE3qjPP0PFCEB/6vPmA2ZCb0xxGRQhMSW7XqHzmImdN9oIpiwDN1txAmLBHGyPkHTzVMUoQ6Wa4bJYDZkOeqejWaoulCGOlmh5XQXIXTPb0wZRaiz43Qn/cUxoRs+M+8RwaSs0TQ+JSWErpRo4U0UIUm3UgKWo24416RRhKRl6X56jJkweeXjpKlmDYVIwUTN2bUk3VJAPZgJk3C6SacIKZrgVOZlTKcOzIRJzYMvEEEXvG6IIDNhkrYUnJnbnjKkbLNmMRfWgitmajGJCLqilV7g6hmWo8koMs9RBJfcxyOgCGEy3lZbiuCSTnp+HVXgmLDO2ptTKIJrlmgVvcZMWDf3E0FXddPjXMLGTFg3HUwPiuCq1VpEtzETJu4VIui6jnqLuZCZMHF5phlFcB1PJWUmTNhcIuiJRlrIXEgIE/MvSuCRlygBIUzMa5TAI69SAo4JE3G/WUgRPPMzXUvPMRMezAeUwEOTKQEhPJh15mOK4GnDlY+jCoSwVtP4FnpPpWnGKKrAMWGt+O5Br/GIfGbCWhUSQc9l8KwZQlib9yiBD2bkUgNCeEDvUwIfTKUEhPBAPjX/RxF8wMNmCOEBLaMEvuinBVxDSgj3bzYl8MlMSkAI928BJeDtjhAGiwfes/AnhIGaY35KEXzSSys5KiSE++LeCT99SAkI4b6KKYGPVlECQriv+ZTAz8U/JSCE+6qgBD6qogSEcG9reMihr3JVwkczhLCmjZTAZ1soASGsaT0l8NkmSkAIa1pNCag4IQwWH5n77SNKQAhZHAWrhBIQwpq+oAQ+20wJCOGeqnK+oQg++4oSEMI9bVvXmSL4rAkPfCKEe/pSmRTBZ131NQ98IoS7fU0JAnnrAyHchQc88dZHCAPGZYxB+J4SEMLdeC47ISSEAfuWEgSgISUghJQBdB9liLUfKAHdRxkIId1HGQC6jzIE51BKQPft1oASBKARJSCEu9WnBL7bqMMoAiHcrfkkauC3bLXmGglJXCqyS5bpRxF8NVub6D5mwj2xNPIbN48Rwr00pQR+HwJQAkK49xEKfD4AoASEsKb2lMBn7SgBIaypAyUghIQwWEdTAp+1oASEkPflYLWiBISwpsyqQorgoxnK5PvJCeFey9GM7RTBR1lqzhdCEsK9cZKCahPCgB1PCXzUlhIQwn11ogQ+OpESEEJCGKyTKQEh3Fd7zaIIPlmm7txBQQj3leVwNaNfWlICQniguRD+6EgJCOH+9aQEPjmVEhDC/ePssV9OogSEcP9y+GjGF3N07GCqsBufUdVwhjmKInjua71G3zETHkgfSuCD3pSAEB5YF0rgAy6LIIS1GMYyyQdnUWVCWJsNlMBjlZSAENbuPErgsbMpASGs3RBK4LGBlGAvrM73kW36UgTPzNUn9Bwz4cH0ogQe4g2OEHLMErDzKQEhZCYM1oB7qAHHhAng4jWvfKkpdBwzIcctQTqNEhDCxAylBB45kxIQwsR0c3gatxc2qB2LUUKYqHMpgQcuoQSEMHFnUQIPnE4JCGHiTnZKKYLLPlcHFqOEsC54FJHb8igBIaybGymByy6iBISwbrKdLyiCi75WNotRQlhXP6cELrqUEhDCuvuZVlIElyzVcOZBQlh3bRxu8HULzysghEniIiu3DKMEhDA5A5wqiuCCz3UKi1FCmKxzKIELLqAEteId6iAuMdQgNTP0GV3GTJiKFpQgRVdSAkKYmusoQUoqdAVFIISp6eR8ThFS0FhdWIwSQubCIP2KEhwU71IJ6GVaU4SklGk6HcZM6IZrKUFS8qkcM6F7WhueRlp3K7WM/mImdENlnnQrZUjC7ZSAELojY7p0gT6kEHW0SIN52jbLUTfdbZZThDrpqtvpLmZC95aj0tVVcylFHSzX1QupAjOhy/5sVlCEhPXXr+gtZkK3XapFFCFBK7iDkBB6oa1zPUVI0O/UgnmQ5ag3skw/inBQi7SavmIm9Eb5uPspQgIepgTMhN7ZaM5SZ0nVSqMYB1ClZ9SSvmIm9Eq2M1qSiGAtfk0ECaG3hnN/4YHXCZKks4kgIfTaaEpwoHWC+A4PQuiLPk7TGu/82K2tejMP1hklS8J6M0TdKcM+VupttaKjmAn90MbhoQ37M5IIMhP6aYjJoAg1bNYsuomZ0E//Qwn28hdKQAj9NdhpRhF2qpaUpV7Mg4TQb2MHL6UIkqQ0rdKoSdSBY8IAPG2mUQRJ0hU6g05iJgzCL50mFEFSNhFkJgxOsRmm42NegzWawtWizITBaevcpOqY12AMESSEwbrGOSbW+3+sevNlHSxHg7bJnK+cmO77J5qgY+ghZsKgZTnjYrvvdxFBQmiHgc4psdzv3hrgVIxg/FmOWuLn5pAY7e1MDdD3mkj3EEKrjo7M6eoao/1drPeURfewHLXJMc59sdrfvxNBQmifoU48ZsIKSX01kAgSQhtde09DKfIn7zPVWDcQQY4JbbXJXKko3+JUrTSV6H26hpnQXlnOA5od4YVomj7WEwwzIbTZZtPVeT7CC1HpLh3PPEgIbdbKkQY6Z0paFsn9O0tnEkFCGAaXOV0jec6wu/6LCBLCsLjdaaaoPRz4SI3cI4JcruYe3tk8c72J0rdWZOhheoWZMGzuuu7QyOxLfSLITBhOheYefRWB/fhar9EnzITh1MEZG9rrZwp3/eorIkgIw6sqJ9t5VKXhfAPZ+e8vNdmRpPJxjCfL0RBFL73ox3/ucLY5PKT78p1epkMIYTRcYsK41YfoWfqD5Wg4lU3c+79McI4N3V6017NOzX3h3CAzYQgXpXv6s1kRou3vplHO7vBlPs6IEsJIeMa8G5ItHaKr6AxCGE1TzL/2+F2FMi3cxkI9ra5OsWlLb3BMaKvKvOT/7lnOGJXs+p1NEczf+e9NekVdHYkIEkKLZUyXpE1Jft7ZyZl8XQML9+pUSVJDvTKpNfFjOWqvsonbhq9UufK1TSX6m05KqnpVOelFT5r3LNy/frq+xh6VVh6dkejfXWxuVjMdoYFqoTbqQF8RQjctMyu1SoVao2yl7fHfr1ZeCtWba25XtkV7WaVx6pbC/rxlnt3jdzPVQp3UVa31UzW5js9WCWGSs97i4fO1RnP00wP8ics1NKXqVeWMXbfFkr1tp7tS7ITnzZsH+D8LlKte6qH2VYnPq4Qw5grM25qu7w86T/V14cF/75sL1S/g/d2gsTot5T0ZbYoO+me2aqDOUFPCSAgPpDJv8rTXaiw4a1dfz7tSvd+bdYEeBw6/p+ltqf+cC0yid0/O1UANU2+iSAhrfgAxPf1FfVPHUwbFmjp4xyelqVpsxqhxAPtt9Cc1UrYrPdDW/KROf36Wemm48jheJIQlZpHeUHkd5r89Xebis8feMGN1nJ9vPBqtQa5t/Zsm2Qc9Hqa82D8+Ksa7v8q8oGfUN4WfcJT+7mr9njTj1dmHPV+jUbrQ1S0fZhql8LfnaZguVw+HEMbKBPOkmqf8UxbpbdevLHnR/FNHebjnRbpN57m8zUVmtAs/ZZNu0MWx7MfYXTFTNvFe08VMdiGCUne97Pr2Xey85VwrL24/rNahukDznfMct++Sf8iVn5Klf6uDGW+Wmbj1ZEzeecrHNb1NWmX+rYdcPSWwVC/pRE9quNxM0BR1cW0BepHOUSdPtnS+OU/9Xfx5S3SpLovVtaux2dVC86BmevBc7BN0vUufke53wWse0SJ1TOEnFOh8XZLStTAHM9K4/5Dj2bpSN6qVQwgjY50Zr4menRS/QX0dr99A5ukdfaC6nAT4QH00VD10nMfb9pJ5yqO7QSp0qm5pl15ECEOqMu/H2ekz84yeVg8PX2uD5vlSxYoRGx77VPnaomJt0Lc773v40Qw11hE6TG3VUT11oktn/w6up8nx8KfP1h80bL+n9aN0t3/EZ8IJZoxyPX+VND0VSB1LK79Nr9IhaqhD1XBSswv9eM0tBc33eEe72nj/XNVNGuPiGU1C6KuPzW1q5NNrnaTbYvXh+o55aKxZ7dPrfa4nlO2UTfTnjYYQumSUKfH19Xrrxpid43rEzPH19Y7TnU7NQ42oiOR5wtdNN1Pi8wPo39czsTq/9aDPEZTWqKeZbn58sgEhtNpI85I6S0leD5q8dzXVxOVh8feZ+QG8ao6e0mizzkTtCaiRWkKVVpakn68+AW5Bf/0qBovScWZpgK++SSN1tkMILXW3WR74NrTS7a7cn2eva822wLfhx+NDlqNWWWWGWhBBabNGjFq1x9FhKg9ItM8nZpAFEZTWqL9Zu6vKYT8IiEgIXzR5OlIVVmxLQw3Va7saJEofI7xgbnXlwvfUVOxccfTR5J1VbnrblgKWowG7wxRbt01N9MdIPcBhvblV9a3bqo4aG4EODv1MWGQusjCC0lb1S/9nZE5aPGp6WRhBabUujECNQ/4+UmCu9ODOCPcY3aJTQl7jqWa8jrR4+5bovZDfbxHqmfBtc66FEdxxkcCyne9xD+i3Zmno3q2rdl6VvcZcY0ZaHUGpmy7S4lDPhyF+B3kwkBPGyTlS/+35TUVuW2Ee1ueh2dozdZlDCH0Wrq/clGarv65y4SG7/phl/qFK3685Sk0P3eIQQh/92lRa+u1+tR7BKkujfLzXLznPmSeUFcquOFoP1qhsWO66CGUI/biLzTvzdK4ut/LjmtnmDc1Qh1BWNV+nSjpcT4Swo0O4yb802xV+H+lSDVNHS+q/zLykN1x7rFSQmuohhxAyCyasQtUaqlwNCXAUppt3NEXdFB1H6lGHEHpmS8Efcyu00arv9XPDfOVqkHp79PDE/Vtj5ulNLTjgF7+FWboecQihJ9abJxTtR2/lK0dXqqVyPHvq5mZTpMWapoWBfyGbt9z+ggJCuNPvTKniYaO26Fh1Vle11uCUR2iTWacVWq11KlR3xUWYZsPQbOid5mPFUb5+UKZaKkstlak0tVN9HapGaqzGNS4QL638Mn2bvpb0hcpUpnKV6lN9rs/2ejRiHFQrTdIJGu0QQhc9ZmZqbqD3zNvYagUyqifpBzXSCSE7te69GbpBf3AIoUveMf+gp1Bnc3WPLg1Bh4fgAu4lRBBJ6aORei8El3Zb/z6x0lzh6zfYIlpK9JxyLO/yBnZvXvm4a4ggUnCs7mA5mootBeNHNaKPkOJi715DCJM/sM5dTQ8hZYv1liGESck3k+gfuOJZfWQIYRJ+S+/ANTftemSHjQtmS91h5TPUEF5t9BdLu93SmfANIgiXrddEQwgTVmJYisJ9f1LZRJajCbrSfBvBuwYRvAw9bGHHWzgTvma+lYggPFCpCYYQHtRnLEXhob9qg3UxtG5yvsJ8R6fAQ831N6cqJ92ihzRYNhO+a2bSJfDUK5pl0q16ToplM+EQk0GXwGPb9bpVfW/VTPgcEYQPGulZQwj3qyrnz/QHfDHWqq8xt2havt2spzvgky76vTW9b81MuJAIwkePaIUhhHt5lL6Aj/prAseENS012+gL+OpFrbdkLrRkXfwL8zVdAZ/Z8pRuK2bCpUQQAajSakMId3qCfkAgnuaYcId5poJuQCBetmIurGdDIYBgnKo3mAmlYvMuvYDAPG/B+cLAQ/iWTqQTEJjOWhvvmbBihHQffYBA3RXvEGY+PtX8hC5AoLZrmoltCCvzpMfoAQSsq56J70yYMX2D2U4PIHBLtaUgtsvRl5VJB8CCuXBqboxDCNjgn3Fdjk43bRh9WKGFFphYhvBDxh7WmBXPmfBFRh7WmBTHEC4whzHysMYnWmxiF8K31YGRhzX6K7hrmAML4TTGHVaZEbcQfmayGHVYZUlg314YUAjfZ8xhmYGaPzxWIZzCmMM6b8drOcpMCPvkxymEH5lcRhzWaaePTGxCuIrxhpUK4zMTzma0YaX34xNCrhqFnebEJYRrzfeMNqzURptNLEL4EReswVpBfF4RQAiXMtKw1pJ4hHADIw1rFccjhHMZaTATBhtCvg4U9iqLQwhXmb6MNKzVXYUm8iFcyzjDauujPxMWMsqwWlH0Q8hMCLutjX4ISxhlsBwNNoR8NTbsVh79EK5nlGG1suiHkAc8wW5fRT2Emw0Xb8Nu36q0MtIh5GoZ2K6ftqVHOoTVjDGs92W0l6PMhAjDgjTSIfyCEYb1vo92CL9ihGG976Idwq2MMKy3MdohNIwwrPdDtEMIcEzITAhY1qU+h9BhhGE9hxACwapPCIFgHUIIAWZCD7VmhGG95tEO4SGMMFiOBhvCNEYY1jss2iE8nBGG9RpHO4RHMMKw3Ezfu9TnEGY5fEsv7Jahlk6kQyhlMsqw2lG+v6LvIeSoEBwRBhzCjowyrNYx+iHMZpRhtWOjH8JujDKslhv9EHZllGGxap04ye/XDOCK6uPNSYw1LLVcK3zPRACPt2AuhL1OCOA1AwhhT0Ya1uoVjxAez0jDWifGI4S59/BFobBTtQY6sQhh09u4agZ2ahLIqwby3NELGG1YaVh8QtiP0YaFCgO6lCSQEB7jfM6Iwzrt1c6JTQilqxhxWGd4QK8bUAgHVC1hzGGVjwM5RxhgCI/OGMSowyqDfL+jPuAQSpdI4nwh7HFdYK8cWAh7O0fyqAtY4yh1cGIXQj6cgU1uDvC1AwzhT5xGjD2scLiOc2IZQukm5TP+CNwi/T7Q1w80hCc7v6ADELgb1dqJaQircqQbBvMJKYK1XRdPCnYLAv/CwEXmcu61R4DGqFPAKagXdAm6OyPoAwRmuJoPDnobrPjq3BtMFd2AALTUrWrhEEJJ0ukmnY6Azyr0rhX9X8+OcjypMnoCvvpEL9xjx5Y4tpSkKueidVzGBr8U60Nret+xpyxVOUPWtaM74MssONeizq9nTwTTi96ctF2z6BB4ehwoVVkVQatmwh1GmPXcXQEPtdY4y7q+nm0letw5m9kQnrlGt0yybZscGwtVaH4TwJcWI+q2a7yyLez4ejYWq4PzlnMO8yFcOwqUFmiwXneyrZx0GthZuLKJzRyppfmUHkKKMiW11IJ26UW2bqFjdwEXmwc0TQMlbeSLtpGEjeqgG9Xd6j537C9jgXleC4kg6uxDddEYnWJ9j1u+gWUTm10oSSXmVT0ZyNdWIZyW61JdrBwnDNvqhKmwk82b2kp/oVb5ytMFOi1Ene2Er8hvmZf0H/Wm27CXFequq9Wx6uiMcG23E9aCLzYzNEVH03nQRqVpiPrqFEeqzMuYHrbtd8I+AAvNCv1HHyiXXoyZalWqvU5VR3UO7AH2hLCGsomrhy9RsdaqQpvVkx6NpNlqqSx11vFqp2N0bES614nmYK0161WoYlVojb7RNm1VXzo4dDPdYjVSKx2u5mql1spRW7WNZL86cRnST80X2qZvVKXt+lJfaqsqVamtKpUk/aAf9L2+l1Gp6usoZWjHsf12spCivZ+yvlD11UCH6BA1UD05aqCGaqJMNVWGmugwZeowHaEmOlyHL2zeIx41cmgTeKkqp+bv7b14DAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAil/wdMe9MwcksORAAAAABJRU5ErkJggg==" alt="username-icon"></div><input id="username-input" class="login-input" placeholder="Username" type="text"></div></div><div id="password-container"><div id="input-box"><div id="icon-container"><img id="password-icon" placeholder="Password" src="/static/media/password_icon.68a83cee.png" alt="password-icon">' +
            '</div><input id="password-input" class="login-input" placeholder="Password" type="password"></div></div>' +
            '<button class="button-login" type="button">Log in</button><button class="button-login" type="button">Back</button></form>'
        );
        await page.click('input[id=username-input]');
        await page.type('input[id=username-input]', 'Larry_Wheels');
        await page.click('input[id=password-input]');
        await page.type('input[id=password-input]', 'larry');
        await page.click('Button[class=button-login]');
        await page.waitForSelector('.startpage');
        let loginHomeHtml = await page.$eval('.startpage', e => e.innerHTML);
        await expect(loginHomeHtml).toContain(
            '<button id="createroom-button" class="startButton">Create a room</button>'
        );
        await page.click('Button[id=createroom-button]');
        await page.click('input[id=roomname-text]');
        await page.type('input[id=roomname-text]', 'TestRoom');
        await page.click('input[id=roomname-button]');
        await page.waitForSelector('.col-sm-4');
        let roomHtml = await page.$eval('.col-sm-4', e => e.innerHTML);
        await expect(roomHtml).toContain('<div id="chat-input-box">' +
            '<div class="button-container"><button id="chat-input-send">Send</button></div>' +
            '<div class="input-container">' +
            '<input id="chat-input" type="text" placeholder="your message..." value=""></div>' +
            '</div>');
        await page.click('input[id=chat-input]');
        await page.type('input[id=chat-input]', 'Hey chat!');
        await delay(500);
        await page.click('Button[id=chat-input-send]');
        await page.waitForSelector('.flex-item-chat');
        roomHtml = await page.$eval('.flex-container-chat', e => e.innerHTML);
        await expect(roomHtml).toContain('<div class="flex-item-chat">Larry_Wheels: Hey chat!</div>');
        browser.close();
    }, 500000);
});

function delay(time) {
    return new Promise(function(resolve) {
        setTimeout(resolve, time)
    });
}