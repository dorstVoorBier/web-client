import React from 'react';
import './Settings.css'

export class Settings extends React.Component {
    constructor(props) {
        super(props);
        this.routeChangeBackHome = this.routeChangeBackHome.bind(this);
        this.removeAccount = this.removeAccount.bind(this);
    }

    removeAccount() {
        fetch(
            'https://ip2-maffia.herokuapp.com/users/delete?username=' + localStorage.getItem("username"),
            {
                method: 'DELETE',
                headers: new Headers({
                    'Authorization': 'Bearer ' + localStorage.getItem('JwtToken'),
                })
            }
        ).then((res) => {
            if (res.status === 200){
                this.props.history.push("/");
            }
        })
    }
    routeChangeBackHome(){
        let path = `/home_login`;
        this.props.history.push(path);
    }
    render() {
        return (
            <div id='settings' className='settings-box col-md-4'>
                <div className='settings-header'>
                    <img id='settings-logo' src={require('../Resources/Images/maffia_logo2.png')} alt='logo' className='header-icon'/>
                </div>
                <div className='settings'>
                    <h2 className='settings-title'>Settings</h2>
                    <form className="settings-form">
                        <label className="labels">Notifications:</label>
                        <div><label className="labels" id="yes-notifications">yes</label><input className="input-radio"  type="radio" name="notification" value="yes" checked/>
                            <span className="checkmark"/>
                        </div>
                        <div><label className="labels">No</label><input className="input-radio"  type="radio" name="notification" value="No"/>
                            <span className="checkmark"/>
                        </div>
                    </form>
                    <form className="settings-form">
                        <label className="labels">Friend Requests:</label>
                        <div><label className="labels">yes</label><input className="input-radio" type="radio" name="friend" value="yes" checked/></div>
                        <div><label className="labels">No</label><input className="input-radio"  type="radio" name="friend" value="No"/></div>
            </form>
                    <input className="settings-input" placeholder='placeholder' type='text' id='placeholder1'/>
                    <input className="settings-input" placeholder='placeholder' type='text' id='placeholder2'/>
                    <input className="settings-input" placeholder='placeholder' type='text' id='placeholder3'/>
                    <button className="back" onClick={this.removeAccount}>Remove Account</button>
                    <input onClick={this.routeChangeBackHome} type='button' className="back" value='Back'/>
                </div>
            </div>
        );
    }
}