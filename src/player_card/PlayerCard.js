import React from 'react';
import './PlayerCard.css'
import {Card, CardImg, CardText, CardTitle} from "reactstrap";

export class PlayerCard extends React.Component {
    text = 'Please click to show role.';//uit db gelezen
    role = '';
    btnShow = 'Show Role';
    btnHide = 'Hide Role';

    constructor(props) {
        super(props);
        this.state = {
            text: this.text,
            playerRole: "",
            btnText: this.btnShow,
            isReady: false
        };
        this.ready = this.ready.bind(this);
    }

    ready() {
        this.setState({isReady: true})
    }

    /**
     * Checks if the role is valid. then gets the role info and updates the state
     * @param prevProps
     * @param prevState
     * @param snapshot
     */
    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.props.playerRole !== "Role was not found, playername was invalid") {
            if (this.props.playerRole !== null && this.role === '') {
                fetch('https://ip2-maffia.herokuapp.com/roleinfo/' + this.props.playerRole,
                    {
                        method: 'GET',
                        headers: new Headers({
                            'Authorization': 'Bearer ' + localStorage.getItem('JwtToken'),
                            'Accept': 'application/json',
                            'Content-Type': 'application/json'
                        })
                    }
                ).then((response) => {
                    response.json().then((res) => {
                        let text = JSON.stringify(res.data);
                        text.replace('"', '');
                        this.role = this.props.playerRole;
                        this.text = text;
                    });
                })
            }
        }
    }

    showRoleInfo = () => {
        if (this.state.btnText === 'Show Role') {
            this.setState({
                playerRole: this.role,
                text: this.text,
                btnText: this.btnHide
            });
        } else {
            this.setState({
                playerRole: '',
                text: 'Please click to show role.',
                btnText: this.btnShow
            })
        }
    };

    render() {

        return (
            <Card body className="player-card">
                <CardTitle>Player name: {localStorage.getItem("username")}</CardTitle>
                <CardImg top width="100%"
                         src={require('../Resources/Images/cards/89848-1532336916.jpg')} alt="card-front"
                         id="card-image"/>

                <CardText>{this.state.playerRole}</CardText>
                <CardText>{this.state.text}</CardText>
                <div>
                    <button className="btn-dark col-sm-5 card-knoppen"
                            onClick={this.showRoleInfo}>{this.state.btnText}</button>
                </div>
            </Card>
        );
    }
}