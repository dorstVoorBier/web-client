import React from 'react';
import './Profile.css'

export class Profile extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            gamesPlayed: 0,
            gamesWon: 0,
            citizenGames: 0,
            citizenWins: 0,
            maffiaGames: 0,
            maffiaWins: 0,
            doctorGames: 0,
            sherrrifGames: 0,
            doctorProtectCount: 0
        };
        this.changeRouteHome = this.changeRouteHome.bind(this);
        this.loadData = this.loadData.bind(this);
        this.loadData()
    }

    changeRouteHome() {
        let path = `/home_login`;
        this.props.history.push(path);
    }

    loadData() {
        fetch('https://ip2-maffia.herokuapp.com/users/user/' + localStorage.getItem("username"), {
            method: 'POST',
            headers: new Headers({
                'Authorization': 'Bearer ' + localStorage.getItem('JwtToken'),
            })
        }).then(response => {
            if (response.status === 200) {
                response.json().then(res => {
                    this.setState({gamesPlayed: res.profile.gamesPlayed,
                        gamesWon: res.profile.gamesWon,
                        citizenGames: res.profile.citizenGames,
                        citizenWins: res.profile.citizenWins,
                        maffiaGames: res.profile.maffiaGames,
                        maffiaWins: res.profile.maffiaWins,
                        doctorGames: res.profile.doctorGames,
                        sherrrifGames: res.profile.sherrrifGames,
                        doctorProtectCount: res.profile.doctorProtectCount
                    })
                });
            } else {
                this.setState({playerRole: "Role was not found, playername was invalid"});

            }
        })
    }

    render() {
        return (<div className="col-md-6" id="profilestats">
                <div className="row">
                    <div className="profile-title">Games played: </div><div className="profile-counter"> {this.state.gamesPlayed}</div>
                </div>
                <div className="row">
                    <div className="profile-title">Games won: </div> <div>{this.state.gamesWon}</div>
                </div>
                <div className="row">
                    <div className="profile-title">Games as Citizen: </div>{this.state.citizenGames}
                </div>
                <div className="row">
                    <div className="profile-title">Games won as Citizen:</div> {this.state.citizenWins}
                </div>
                <div className="row">
                    <div className="profile-title">Games as Maffia: </div>{this.state.maffiaGames}
                </div>
                <div className="row">
                    <div className="profile-title">Games won as Maffia: </div>{this.state.maffiaWins}
                </div>
                <div className="row">
                    <div className="profile-title">Games as Doctor: </div>{this.state.doctorGames}
                </div>
                <div className="row">
                    <div className="profile-title">Games as Sherrif: </div>{this.state.sherrrifGames}
                </div>
                <div className="btn btn-dark" id="btn-profile-back" onClick={this.changeRouteHome}>Back</div>
            </div>


        );
    }
}