import React from 'react';
import {configure, mount} from 'enzyme';
import {Login} from './Login';
import Adapter from 'enzyme-adapter-react-16';

configure({adapter: new Adapter()});

describe('Login', () => {
    it('should render and have appropriate fields', async () => {
        const login = await mount(<Login/>);
        let html = await login.html();
        expect(login.html().includes(
            '<div><div id="login-container" class="col-md-4"><div id="login-logo">' +
            '<img id="logo" src="maffia_logo2.png" alt="logo" class="header-icon"></div>' +
            '<div id="login" class="login-box"><h2 class="login-title">Login</h2>' +
            '<form class="login"><div id="username-container"><div id="input-box">' +
            '<div id="icon-container"><img id="username-icon" src="user_icon.png" alt="username-icon"></div>' +
            '<input id="username-input" class="login-input" placeholder="Username" type="text"></div></div>' +
            '<div id="password-container"><div id="input-box"><div id="icon-container">' +
            '<img id="password-icon" placeholder="Password" src="password_icon.png" alt="password-icon"></div>' +
            '<input id="password-input" class="login-input" placeholder="Password" type="password"></div></div>' +
            '<button class="button-login" type="button">Log in</button>' +
            '<button class="button-login" type="button">Back</button></form></div></div></div>'
            )
        ).toEqual(true);
    });
});