import React from 'react';
import './Login.css'


export class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: null,
            password: null,
            token: null
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.changeRouteBackHome = this.changeRouteBackHome.bind(this)
    }

    changeRouteBackHome(){
            let path = `/`;
            this.props.history.push(path);
    }

    async handleSubmit(){
        fetch('https://ip2-maffia.herokuapp.com/login', {
            method: 'POST',
            headers:{
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                "username": this.state.username,
                "password": this.state.password
            })
        }).then( (resp) => {
            if(resp.status === 200) {
                this.setState({token: resp.headers.get("Authorization").replace('Bearer ', '')});
                localStorage.setItem('JwtToken', this.state.token);
                localStorage.setItem('username', this.state.username);
                this.props.history.push('/home_login')
            } else {
                alert('Something went wrong, status code: ' + resp.status)
            }
        });
    }

    render() {
        return (
            <div>
                <div id='login-container' className="col-md-4">
                    <div id='login-logo'>
                        <img id='logo' src={require('../Resources/Images/maffia_logo2.png')} alt='logo'
                             className='header-icon'/>
                    </div>
                    <div id='login' className='login-box'>
                        <h2 className='login-title'>Login</h2>
                        <form className='login'>
                            <div id="username-container">
                                <div id="input-box">
                                    <div id="icon-container">
                                        <img id='username-icon'  src={require('../Resources/Images/user_icon.png')} alt='username-icon'/>
                                    </div>
                                    <input id="username-input" className="login-input" placeholder="Username" type="text" onInput={e => this.setState({username: e.target.value})}/>
                                </div>
                            </div>

                            <div id="password-container">
                                <div id="input-box">
                                    <div id="icon-container">
                                        <img id='password-icon' placeholder="Password" src={require('../Resources/Images/password_icon.png')} alt='password-icon'/>
                                    </div>
                                    <input id="password-input" className="login-input" placeholder="Password" type="password" onInput={e => this.setState({password: e.target.value})}/>
                                </div>
                            </div>
                            <button className="button-login" type="button" onClick={this.handleSubmit}>Log in</button>
                            <button className="button-login" type="button" onClick={this.changeRouteBackHome}>Back</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}