import React from 'react';
import './PlayerDead.css'
import {Card, CardImg, CardText, CardTitle} from "reactstrap";

export class PlayerDead extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: this.props.name,
            playerRole: this.props.role,
            gameId: this.props.gameId
        };

    }

    render() {
        return (
            <div>

                <Card className="player-dead" body
                >
                    <CardTitle>{this.state.name}</CardTitle>
                    <img id="skull"
                         src={require('../Resources/Images/kisspng-skull-and-crossbones-cartoon-clip-art-skull-5a8054801d5015.8821880415183596801201.png')}
                         alt="skull"/>
                    <CardImg top width="100%"
                             src={require('../Resources/Images/cards/89848-1532336916.jpg')} alt="card-front"
                             id="card-image"/>

                    <CardText>{this.state.playerRole} </CardText>
                </Card>
            </div>

        );
    }
}